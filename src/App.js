import React, { useState, useRef, useEffect, useCallback } from "react";

//Global components
import { Breadcrumb, Crumb } from "./components/Global";

//Questionnaire components
import {
  Button,
  Checkbox,
  Input,
  Question,
  Questionnaire,
} from "./components/Questionnaire";

//Result components
import {
  ButtonLink,
  ContactEmail,
  ContactPhone,
  ContactUs,
  EmailForm,
  EmailInput,
  Matrix,
  Results,
  SubmitButton,
} from "./components/Result";

//Functions used to calculate results of user's inputs. Used in the handleResult-function
import {
  yearlyHoursSaved,
  yearlyHoursFormatted,
  yearlyMoneySaved,
  yearlyMoneyFormatted,
  returnOnInvestment,
  calculateProcessLength,
  calculateAppNum,
  calculateAppTypes,
  calculateComplexity,
  benefitProcessLength,
  benefitProcessNum,
  benefitProcessEmpl,
  benefitProcessCost,
  calculateBenefits,
  calculateBenefitsRounded,
} from "./functions/handleResult";

function App() {
  //These states hold the data used for questions and input elements
  const [calcData, setCalcData] = useState([]);
  const [checkboxData, setCheckboxData] = useState([]);

  //Sets which question is active
  const [questionNum, setQuestionNum] = useState(1);

  //value and onChange states for input fields
  const [input, setInput] = useState("");

  //value and onChange states for input field that is rendered when the user checks the checkbox "andre"
  const [programNum, setProgramNum] = useState("");

  //this state holds the amount of checkboxes checked for validation purposes.
  const [isBoxChecked, setIsBoxChecked] = useState(0);

  //States used for validation
  const [isEmpty, setIsEmpty] = useState("");
  const [validate, setValidate] = useState(false);
  const [validateMSG] = useState("Feltet skal udfyldes");

  //These states holds calculated data to be displayed in the Result component
  const [hoursSaved, setHoursSaved] = useState(undefined);
  const [moneySaved, setMoneySaved] = useState(undefined);
  const [ROI, setROI] = useState(undefined);

  //These states holds the value used as data in the Matrix component
  const [complexityResult, setComplexityResult] = useState(undefined);
  const [benefitResult, setBenefitResult] = useState(undefined);

  //value and onChange states for EmailInput component
  const [email, setEmail] = useState("");

  //State used for animation purposes
  const [toggleAnimation, setAnimation] = useState(false);

  //Ref used to focus on input fields when they are mounted
  const inputRef = useRef(null);

  /* Fetch data from json files in public folder and add the data to useState hooks ---> */
  const getData = useCallback(() => {
    fetch("calcData.json", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((json) => {
        setCalcData(json);
      });
  }, []);

  const getCheckboxData = useCallback(() => {
    fetch("checkboxData.json", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((json) => {
        setCheckboxData(json);
      });
  }, []);

  useEffect(() => {
    getData();
    getCheckboxData();

    /* return (cleanUp = () => {}); */
  }, [getCheckboxData, getData]);

  /* <--- Fetch data from json files in public folder and add the data to useState hooks */

  //Set focus on input field when Input component mounts
  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  });

  //Clear validation messages when user starts typing in input fields
  useEffect(() => {
    if (!isEmpty) {
      setValidate(false);
    }
  }, [isEmpty]);

  //This handler fires when the user clicks the Crumb component in the Breadcrumb component
  const handleClickCrumb = (data, value, index) => {
    if (data.id !== 2) {
      if (value) {
        setInput(value);
        setQuestionNum(index + 1);
      }
    } else {
      if (value.length > 0) {
        setInput(programNum);
        setQuestionNum(index + 1);
      }
    }
  };

  //Input onChange handler
  const handleInputChange = (e) => {
    setInput(e.target.value);
    e.preventDefault();
  };

  //Checkbox onChange handler
  const handleCheckboxChange = (e) => {
    setCheckboxData([
      ...checkboxData.map((ch) =>
        e.target.id === ch.id ? { ...ch, checked: e.target.checked } : ch
      ),
    ]);
  };

  //Button handler when the user click the button "Næste"
  const handleClickNext = async (event, data) => {
    if (isEmpty) {
      setValidate(true);
      event.preventDefault();
    } else {
      if (data.id !== 2) {
        setAnimation(true);
        data.input[0].value = input;
        setTimeout(function () {
          setQuestionNum((prevState) => prevState + 1);
          setValidate(false);
        }, 1000);
        event.preventDefault();
      } else {
        setQuestionNum((prevState) => prevState + 1);
        setValidate(false);
        event.preventDefault();
      }
    }
  };

  //Button handler when the user clicks the button "Beregn ROI" - calculates input data
  const handleResult = (empl) => {
    //string to numbers
    const durationInt = parseInt(calcData[0].input[0].value);
    const programNumInt = parseInt(programNum);
    const timesWeeklyInt = parseInt(calcData[2].input[0].value);
    const hourlyRateInt = parseInt(calcData[3].input[0].value);

    //ROI
    const hoursSaved = yearlyHoursSaved(durationInt, timesWeeklyInt, empl);
    const hoursSavedFormatted = yearlyHoursFormatted(hoursSaved);

    const moneySaved = yearlyMoneySaved(hoursSaved, hourlyRateInt);
    const moneySavedFormatted = yearlyMoneyFormatted(moneySaved);

    const ROI = returnOnInvestment(moneySaved);

    setHoursSaved(hoursSavedFormatted);
    setMoneySaved(moneySavedFormatted);
    setROI(ROI);

    // Kompleksitetsfaktor
    const complexLength = calculateProcessLength(durationInt);
    const complexAppNum = calculateAppNum(checkboxData, programNumInt);
    const complexAppTypes = calculateAppTypes(checkboxData, programNumInt);

    const complexity = calculateComplexity(
      complexLength,
      complexAppNum,
      complexAppTypes
    );

    setComplexityResult(complexity);

    // Fordelsfaktor
    const benefitLength = benefitProcessLength(durationInt);
    const benefitNum = benefitProcessNum(timesWeeklyInt);
    const benefitEmpl = benefitProcessEmpl(empl);
    const benefitCost = benefitProcessCost(hourlyRateInt);

    const benefits = calculateBenefits(
      benefitLength,
      benefitNum,
      benefitEmpl,
      benefitCost
    );

    const benefitsRounded = calculateBenefitsRounded(benefits);

    setBenefitResult(benefitsRounded);
  };

  //Button handler for when the user clicks the button "Prøv beregner igen" in the Result component
  const handleTryAgain = () => {
    getData();
    getCheckboxData();
    setProgramNum("");
    setCheckboxData([]);
    setQuestionNum(1);
  };

  if (calcData.length > 0) {
    return (
      <div className="d-flex flex-col min-height-100-percent max-height-auto overflow-y-auto align-items-center">
        <Breadcrumb>
          {calcData.map((data, index) => {
            let value = undefined;
            let label = undefined;

            if (data.id !== 2) {
              value = data.input[0].value;
              label = data.short;
            } else if (data.id === 2) {
              let val = [];
              checkboxData.forEach((ch) =>
                ch.checked ? val.push(ch.name) : val
              );

              value = val;
            }
            return (
              <Crumb
                key={`crumb-${index}`}
                onClick={() => handleClickCrumb(data, value, index)}
                active={index + 1 === questionNum ? true : false}
                value={value}
                label={label}
              />
            );
          })}
        </Breadcrumb>

        {questionNum <= calcData.length &&
          calcData.map((data, index) => {
            if (data.id === questionNum) {
              return (
                <Questionnaire
                  key={`questionnaire-${index}`}
                  setAnimation={setAnimation}
                >
                  <div
                    className={`${
                      data.id !== 2
                        ? "just-cont-space-around"
                        : "just-cont-space-around"
                    } d-flex flex-col min-height-42-percent align-items-center`}
                  >
                    <Question
                      question={data.question}
                      active={data.id === questionNum ? true : false}
                      toggleAnimation={toggleAnimation}
                    />
                    <div
                      className={`${
                        data.id !== 2
                          ? "d-flex flex-col"
                          : "d-flex flex-wrap just-cont-center"
                      }`}
                    >
                      {data.id !== 2
                        ? data.input.map((inp, ind) => {
                            return (
                              <Input
                                key={`input-${ind}`}
                                id={data.id}
                                inp={inp}
                                toggleAnimation={toggleAnimation}
                                buttonColor={data.buttonColor}
                                name={inp.name}
                                type={inp.type}
                                value={input}
                                setInput={setInput}
                                onChange={handleInputChange}
                                placeholder={inp.placeholder}
                                label={inp.label}
                                max={inp.max}
                                setIsEmpty={setIsEmpty}
                                validate={validate}
                                validateMSG={validateMSG}
                                ref={inputRef}
                              />
                            );
                          })
                        : checkboxData.map((ch, i) => {
                            return (
                              <Checkbox
                                key={`checkbox-${i}`}
                                id={ch.id}
                                name={ch.name}
                                checkboxData={checkboxData}
                                isBoxChecked={isBoxChecked}
                                setIsBoxChecked={setIsBoxChecked}
                                input={programNum}
                                setInput={setProgramNum}
                                type={ch.type}
                                value={ch.value}
                                setIsEmpty={setIsEmpty}
                                validate={validate}
                                validateMSG={validateMSG}
                                checked={ch.checked}
                                onChange={handleCheckboxChange}
                                label={ch.label}
                              />
                            );
                          })}
                    </div>
                  </div>
                  <div
                    key={`button-${index}`}
                    className="d-flex just-cont-center margin-0-1-2-1rem margin-button-lg"
                  >
                    {questionNum < calcData.length && (
                      <Button
                        id="save-data-button"
                        index={index}
                        type="submit"
                        toggleAnimation={toggleAnimation}
                        buttonLabel={data.buttonLabel}
                        labelColor={data.labelColor}
                        buttonColor={data.buttonColor}
                        onClick={(e) =>
                          handleClickNext(e, data).then(() => {
                            const nextIndex = index + 1;
                            return data.input.value
                              ? setInput(calcData[nextIndex].input.value)
                              : null;
                          })
                        }
                      />
                    )}
                    {questionNum === calcData.length && (
                      <Button
                        id="save-data-button"
                        index={index}
                        type="submit"
                        toggleAnimation={toggleAnimation}
                        buttonLabel={data.buttonLabel}
                        labelColor={data.labelColor}
                        buttonColor={data.buttonColor}
                        onClick={(e) =>
                          handleClickNext(e, data).then(handleResult(input))
                        }
                      />
                    )}
                  </div>
                </Questionnaire>
              );
            } else return null;
          })}
        {questionNum > calcData.length && (
          <Results>
            <h1 className="d-flex font-fam-open-sans color-blue">Resultat</h1>
            <Matrix
              x={benefitResult}
              y={complexityResult}
              ROI={ROI}
              hoursSaved={hoursSaved}
              moneySaved={moneySaved}
            />
            <h2 className="d-flex font-fam-open-sans color-blue margin-t-3rem">
              Kontakt os
            </h2>
            <ContactUs title="Kontakt os">
              <ContactPhone
                text="Ring direkte på tlf:"
                phone="+45 52 78 98 52"
              />
              <ContactEmail text="Eller skriv jer op til en uforpligtende samtale om mulighederne for automatisering af jeres processer.">
                <EmailForm>
                  <EmailInput
                    id="MERGE0"
                    name="MERGE0"
                    type="email"
                    value={email}
                    setValue={setEmail}
                    hoursSaved={hoursSaved}
                    moneySaved={moneySaved}
                    ROI={ROI}
                    placeholder="skriv din email her"
                  />
                  <SubmitButton label="Send" />
                </EmailForm>
              </ContactEmail>
            </ContactUs>
            <ButtonLink
              onClick={handleTryAgain}
              buttonLabel="Prøv beregner igen"
              labelColor="color-blue"
            />
          </Results>
        )}
      </div>
    );
  } else
    return (
      <span className="d-flex just-cont-center color-blue">Indlæser...</span>
    );
}

export default App;
