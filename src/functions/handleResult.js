//Functions used to calculate results of user's inputs. Used in the handleResult-function in the App component

const yearlyHoursSaved = (duration, weekly, employees) => {
  const resultHours = Math.floor(
    duration * (((weekly * 46) / 60) * parseInt(employees))
  );

  return resultHours;
};

const yearlyHoursFormatted = (hours) => {
  let resultHoursFormatted = hours.toString().split(".");
  resultHoursFormatted[0] = resultHoursFormatted[0].replace(
    /\B(?=(\d{3})+(?!\d))/g,
    "."
  );
  resultHoursFormatted.join(".");

  return resultHoursFormatted;
};

const yearlyMoneySaved = (hours, rate) => {
  const resultMoney = Math.floor(hours * rate);

  return resultMoney;
};

const yearlyMoneyFormatted = (money) => {
  let resultMoneyFormatted = money.toString().split(".");
  resultMoneyFormatted[0] = resultMoneyFormatted[0].replace(
    /\B(?=(\d{3})+(?!\d))/g,
    "."
  );
  resultMoneyFormatted.join(".");

  return resultMoneyFormatted;
};

const returnOnInvestment = (money) => {
  const resultROI = Math.ceil(25000 / (money / 12));
  return resultROI;
};

const calculateProcessLength = (duration) => {
  const durationRounded = Math.ceil(duration / 10);
  const complexityLength = Math.pow(1.4, durationRounded);

  return complexityLength;
};

const calculateAppNum = (checked, num) => {
  let checkedNum = 0;

  checked.map((ch) => {
    if (ch.name !== "andre") {
      if (ch.checked === true) {
        return (checkedNum += 1);
      } else return null;
    } else return null;
  });

  let totalNum = 0;
  if (num) {
    totalNum = checkedNum + num;
  } else {
    totalNum = checkedNum;
  }

  const complexityAppNum = Math.pow(1.2, totalNum);

  return complexityAppNum;
};

const calculateAppTypes = (checked, num) => {
  let complex = 0;
  let simple = 0;

  checked.map((ch) => {
    if (ch.checked === true) {
      if (ch.name === "andre") {
        return (complex += num);
      } else if (ch.complex === true) {
        return (complex += 1);
      } else {
        return (simple += 1);
      }
    } else return null;
  });

  let complexityAppType = 1;
  if (complex) {
    complexityAppType *= Math.pow(1.5, complex);
    if (simple) {
      complexityAppType *= Math.pow(1.2, simple);
    }
  } else if (simple) {
    complexityAppType *= Math.pow(1.2, simple);
  }

  return complexityAppType;
};

const calculateComplexity = (length, num, type) => {
  const complexity = length * num * type;
  const complexityRounded = Math.round(complexity * 10) / 10;
  const highestPossible = complexityRounded <= 10 ? complexityRounded : 9.9;

  return highestPossible;
};

const benefitProcessLength = (duration) => {
  const durationRounded = Math.ceil(duration / 10);
  const processLength = Math.pow(1.2, durationRounded);

  return processLength;
};

const benefitProcessNum = (weekly) => {
  const processNumber = Math.pow(1.05, weekly);

  return processNumber;
};

const benefitProcessEmpl = (empl) => {
  const processEmployees = Math.pow(1.05, empl);

  return processEmployees;
};

const benefitProcessCost = (rate) => {
  let processCost = 0;
  if (rate < 250) {
    processCost = 1.2;
  } else if (rate < 400) {
    processCost = 1.4;
  } else if (rate > 400) {
    processCost = 1.6;
  }

  return processCost;
};

const calculateBenefits = (length, num, empl, cost) => {
  const benefits = length * num * empl * cost;

  return benefits;
};

const calculateBenefitsRounded = (benefits) => {
  if (benefits >= 5) {
    return 4.9;
  } else {
    const benefitsRounded = Math.round(benefits * 10) / 10;

    return benefitsRounded;
  }
};

export {
  yearlyHoursSaved,
  yearlyHoursFormatted,
  yearlyMoneySaved,
  yearlyMoneyFormatted,
  returnOnInvestment,
  calculateProcessLength,
  calculateAppNum,
  calculateAppTypes,
  calculateComplexity,
  benefitProcessLength,
  benefitProcessNum,
  benefitProcessEmpl,
  benefitProcessCost,
  calculateBenefits,
  calculateBenefitsRounded,
};
