import { useSpring, animated } from "react-spring";

const Crumb = ({ onClick, active, value, label }) => {
  //Users input will appear below it's corresponding breadcrumb. This sets the string for the crumb.
  //arrayToString will change the array of checkbox values to a string.
  //truncateString will make sure that the string doesn't get too long to fit below the crumb.
  const arrayToString = (val) => (Array.isArray(val) ? val.join(", ") : val);
  const truncateString = (str, num) => {
    if (str.length < num) {
      return str;
    } else {
      return str.slice(0, num) + "...";
    }
  };

  const string = arrayToString(value);
  const newValue = truncateString(string, 10);

  //Animation springs
  const [spring, set] = useSpring(() => ({
    backgroundColor: "#C2C2C2",
    width: "100%",
    height: "9px",
  }));

  set({
    backgroundColor: active ? "#0B3C5D" : "#C2C2C2",
    width: "100%",
    height: "9px",
    duration: 600,
  });

  const [springSp, setSp] = useSpring(() => ({
    opacity: 0,
    color: "#C2C2C2",
    whiteSpace: "break-spaces",
    overflowY: "hidden",
    height: "44px",
    maxHeight: "44px",
    lineHeight: "22px",
  }));

  setSp({
    opacity: newValue ? 1 : 0,
    color: active ? "#0B3C5D" : "#C2C2C2",
    whiteSpace: "break-spaces",
    overflowY: "hidden",
    height: "44px",
    maxHeight: "44px",
    lineHeight: "22px",
    delay: label ? 300 : 0,
  });

  return (
    <div
      onClick={onClick}
      className="d-flex flex-col height-100-percent align-items-center cursor-pointer"
    >
      <animated.div
        className="d-flex border-radius-5px just-cont-center"
        style={spring}
      >
        {" "}
      </animated.div>
      <animated.span
        className="d-flex flex-col width-100-percent just-cont-center align-items-center font-fam-open-sans font-size-0-875rem font-weight-sb text-align-center"
        style={springSp}
      >
        {newValue}

        {newValue && (
          <span
            className={`${
              active ? "color-blue" : "color-light-gray"
            } d-flex font-fam-open-sans font-size-0-875rem font-weight-sb`}
          >
            {label}
          </span>
        )}
      </animated.span>
    </div>
  );
};

export default Crumb;
