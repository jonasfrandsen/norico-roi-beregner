const Breadcrumb = ({ children }) => {
  return (
    <div className="d-flex width-100-percent height-15-vh just-cont-center align-items-center">
      <div className="d-inline-grid grid-template-breadcrumbs column-gap-3-percent width-breadcrumb width-results-lg padding-0-1rem">
        {children}
      </div>
    </div>
  );
};

export default Breadcrumb;
