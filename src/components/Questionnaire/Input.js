import React, { useEffect } from "react";
import { useSpring, animated } from "react-spring";

//assets
import ValidationAlertIcon from "../../assets/icons/error_outline-24px.svg";

const Input = React.forwardRef((props, ref) => {
  const {
    id,
    inp,
    toggleAnimation,
    name,
    type,
    value,
    setInput,
    onChange,
    placeholder,
    max,
    validate,
    setIsEmpty,
    validateMSG,
  } = props;

  //Checks if input field is empty or not. For validation purposes. User cannot proceed with an empty input field.
  useEffect(() => {
    value ? setIsEmpty(false) : setIsEmpty(true);
  }, [setIsEmpty, value]);

  //If a value for the current question already exists, show that value in the input field.
  useEffect(() => {
    inp.value ? setInput(inp.value) : setInput("");
  }, [inp.value, setInput]);

  //Animation springs
  const [spring, set] = useSpring(() => ({
    opacity: 0,
    visibility: "hidden",
  }));

  set({
    opacity: toggleAnimation ? 0 : 1,
    visibility: toggleAnimation ? "hidden" : "visible",
    delay: 1000,
  });

  const [springIn, setIn] = useSpring(() => ({
    visibility: "visible",
    width: "150px",
  }));

  setIn({
    opacity: toggleAnimation ? 0 : 1,
    visibility: toggleAnimation ? "hidden" : "visible",
    width: "150px",
  });

  const [springResult, setRe] = useSpring(() => ({
    opacity: 1,
    visibility: "hidden",
    position: "absolute",
  }));

  setRe({
    duration: 600,
    visibility: toggleAnimation ? "visible" : "hidden",
    opacity: toggleAnimation ? 0 : 1,
    transform: toggleAnimation
      ? "translateY(-32vh) scale(3, 3)"
      : "translateY(0vh) scale(0, 0)",
  });

  const [springPl, setPl] = useSpring(() => ({
    visibility: "visible",
  }));

  setPl({ visibility: toggleAnimation ? "hidden" : "visible" });

  return (
    <animated.div
      style={spring}
      className="d-flex flex-col width-100-percent just-cont-center align-items-center"
    >
      <div className="d-flex just-cont-center">
        <div className="d-flex flex-col align-items-center">
          <animated.input
            id={id}
            type={type}
            name={name}
            value={value}
            onChange={(e) => onChange(e)}
            max={max}
            ref={ref}
            className="d-flex border-style-solid border-radius-21px font-fam-open-sans font-size-1-875rem font-weight-sb text-align-center color-blue"
            style={springIn}
          />
          <animated.span
            className="d-flex font-fam-open-sans font-size-1-875rem font-weight-sb text-align-center color-blue"
            style={springResult}
          >
            {value}
          </animated.span>
        </div>
      </div>
      <animated.span
        style={springPl}
        className="d-flex font-fam-open-sans font-size-1-25rem font-weight-sb color-blue"
      >
        {placeholder}
      </animated.span>
      <div
        className={`${
          validate ? "d-flex" : "d-none"
        } just-cont-center align-items-center`}
      >
        <img src={ValidationAlertIcon} alt="alert-icon" />

        <span className="d-flex color-red margin-l-0-75rem">{validateMSG}</span>
      </div>
    </animated.div>
  );
});

export default Input;
