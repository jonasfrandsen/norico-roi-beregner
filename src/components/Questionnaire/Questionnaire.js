import React, { useEffect } from "react";

const Questionnaire = ({ children, setAnimation }) => {
  //Sets state toggleAnimation, which handles animations, to false when component re-renders
  useEffect(() => {
    setAnimation(false);
  }, [setAnimation]);

  return (
    <form className="d-flex flex-col width-results-lg height-75-vh just-cont-space-between just-cont-flex-start-lg">
      {children}
    </form>
  );
};

export default Questionnaire;
