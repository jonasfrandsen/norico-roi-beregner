import { useSpring, animated } from "react-spring";

const Question = ({ question, toggleAnimation }) => {
  //Animation springs
  const spring = useSpring({
    to: async (next, cancel) => {
      await next({
        opacity: 1,
        visibility: toggleAnimation ? "hidden" : "visible",
      });
      await next({ transform: "scale(0.8, 0.8) translateY(0px)" });
    },
    from: {
      opacity: 0,
      visibility: "hidden",
      transform: "scale(1, 1) translateY(15vh)",
    },
  });

  return (
    <animated.span
      style={spring}
      className="d-flex width-100-percent just-cont-center font-fam-milk-drops font-size-question-responsive line-height-40px text-align-center color-blue"
    >
      {question}
    </animated.span>
  );
};

export default Question;
