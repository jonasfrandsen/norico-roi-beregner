import { useSpring, animated } from "react-spring";

const Button = ({
  id,
  type,
  toggleAnimation,
  buttonLabel,
  labelColor,
  buttonColor,
  onClick,
}) => {
  //Animation spring
  const [spring, set] = useSpring(() => ({
    opacity: 0,
  }));

  set({
    opacity: toggleAnimation ? 0 : 1,
    delay: toggleAnimation ? 0 : 1000,
  });

  return (
    <animated.button
      id={id}
      type={type}
      style={spring}
      onClick={onClick}
      className={`${buttonColor} ${labelColor} d-flex width-100-percent width-auto-lg height-42px just-cont-center align-items-center font-fam-open-sans font-size-1-25rem border-style-none border-radius-21px 
      padding-0-2rem-lg box-shadow-button cursor-pointer`}
    >
      {buttonLabel}
    </animated.button>
  );
};

export default Button;
