import React, { useRef, useEffect } from "react";
import { useSpring, animated } from "react-spring";

//assets
//assets
import ValidationAlertIcon from "../../assets/icons/error_outline-24px.svg";

const Checkbox = ({
  id,
  name,
  type,
  value,
  checkboxData,
  isBoxChecked,
  setIsBoxChecked,
  setIsEmpty,
  validate,
  validateMSG,
  checked,
  onChange,
  label,
  input,
  setInput,
}) => {
  const inputRef = useRef();

  //useEffect calculates how many checkboxes are checked
  useEffect(() => {
    const checkCheckboxes = () => {
      let val = 0;
      checkboxData.map((check) => {
        if (check.checked) {
          return (val += 1);
        } else return val;
      });
      return val;
    };
    setIsBoxChecked(checkCheckboxes());
  }, [checkboxData, setIsBoxChecked]);

  //sets isEmpty state if no checkboxes are checked - this is used for validation. User cannot progress without checking any boxes.
  useEffect(() => {
    if (!isBoxChecked) {
      setIsEmpty(true);
    } else setIsEmpty(false);

    if (name === "andre") {
      if (checked === true) {
        input ? setIsEmpty(false) : setIsEmpty(true);
      }
    }
  }, [checked, input, isBoxChecked, name, setIsEmpty]);

  //Focuses on input field if it appears in the DOM
  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  });

  //Animation springs
  const spring = useSpring({
    opacity: 1,
    from: {
      opacity: 0,
    },
    delay: 1000,
  });

  return (
    <animated.div
      className="d-flex flex-col width-80-px just-cont-space-between align-items-center"
      style={spring}
    >
      <div className="d-flex" style={{ margin: "0.5rem 0" }}>
        <label className="d-flex font-fam-open-sans font-size-0-875rem text-align-right">
          {label}
        </label>
        <input
          id={id}
          type={type}
          name={name}
          value={value}
          checked={checked}
          onChange={(e) => onChange(e)}
          className="d-flex border-style-solid border-radius-21px cursor-pointer"
        />
      </div>

      <div
        className={`${
          name === "andre" && checked ? "d-flex" : "d-none"
        } position-absolute width-100-vw left-0 just-cont-center align-items-center margin-t-2-5-rem`}
      >
        <label className="d-flex font-fam-open-sans font-size-0-875rem">
          Hvor mange andre?
        </label>
        <input
          ref={inputRef}
          className="d-flex width-50-px border-style-secondary-input border-width-thin margin-l-0-75rem font-fam-open-sans font-size-0-875rem text-align-center"
          type="number"
          placeholder="0"
          value={input}
          onChange={(e) => setInput(e.target.value)}
        />
        <div
          className={`${
            validate ? "d-flex" : "d-none"
          } just-cont-center align-items-center`}
        >
          <img src={ValidationAlertIcon} alt="alert-icon" />

          <span className="d-flex color-red margin-l-0-75rem">
            {validateMSG}
          </span>
        </div>
      </div>
    </animated.div>
  );
};

export default Checkbox;
