const Output = ({ output, label }) => {
  return (
    <div className="d-flex flex-col width-100-percent margin-b-0-25rem">
      <output className="d-flex just-cont-center font-fam-open-sans font-size-1-375rem font-weight-b text-align-center">
        {output}
      </output>
      <span className="d-flex just-cont-center font-fam-open-sans text-align-center line-height-25px">
        {label}
      </span>
    </div>
  );
};

export default Output;
