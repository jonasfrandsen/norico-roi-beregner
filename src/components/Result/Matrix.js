import React, { useState, useEffect, useCallback } from "react";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ReferenceArea,
  ResponsiveContainer,
} from "recharts";

import Output from "./Output";

const Matrix = ({ x, y, ROI, hoursSaved, moneySaved }) => {
  const [highlighted, setHighlighted] = useState(undefined);
  const [explainerTitle, setExplainerTitle] = useState("");
  const [explainerText, setExplainerText] = useState("");

  //Calculated results based on user's input.
  const data = [{ x: x, y: y }];

  //Colors, title and text for highlighted and non-highlighted ReferenceAreas in the matrix.
  const colorDefault = "#0b3c5d";
  const colorhighlight = "#FFC501";

  const textColorDefault = "#ffffff";
  const textColorHightlight = "#1a1a1a";

  const titleOne = "Lavthængende frugter";
  const titleTwo = "Hurtig gevinst";
  const titleThree = "Nødvendige forbedringer";
  const titleFour = "Langsigtede forbedringer";

  const textOne =
    "Processen tager ikke lang tid at udvikle, samtidig med at det giver et lavt afkast. Processen kan benyttes som et Proof-of-Concept for RPA teknologi, da det oftest vil tage mindre en uge at automatisere denne.";

  const textTwo =
    "Processen tager ikke lang tid at udvikle, og er hurtigt tjent hjem igen i besparede arbejdstimer. Processer under ”hurtig gevinst” bør være de første der automatiseres,   da det skaber en succesoplevelse både for ledelsen og virksomheden.";

  const textThree =
    "Processen tager meget tid at udvikle, men ligeledes vil virksomheden opnå store besparelser ved at automatisere denne. Processer under ”nødvendige forbedringer” kræver oftest meget dokumentation og procesforståelse for at automatiseres.";

  const textFour =
    "Processen giver et lavt afkast, og vil tage lang tid at udvikle. Disse processer bør udsættes, indtil der ikke er andre processer tilbage og betragtes derfor som langsigtede forbedringer.";

  //If data values lie within a certain ReferenceArea in the Matrix that area will be highlighted.
  const handleHighlighted = useCallback(() => {
    setHighlighted(() => {
      if (x < 2.5 && y <= 5) {
        return 1;
      } else if (x >= 2.5 && y < 5) {
        return 2;
      } else if (x >= 2.5 && y >= 5) {
        return 3;
      } else if (x < 2.5 && y > 5) {
        return 4;
      }
    });
  }, [x, y]);

  useEffect(() => {
    handleHighlighted();

    /* return cleanUp = () => {
      
    } */
  }, [handleHighlighted]);

  //If data values lie within a certain ReferenceArea in the Matrix a title and explainer text for that area will be rendered on the page.
  const handleTitle = useCallback(() => {
    setExplainerTitle(() => {
      if (highlighted === 1) {
        return titleOne;
      } else if (highlighted === 2) {
        return titleTwo;
      } else if (highlighted === 3) {
        return titleThree;
      } else if (highlighted === 4) {
        return titleFour;
      }
    });
  }, [highlighted]);

  const HandleExplainer = useCallback(() => {
    setExplainerText(() => {
      if (highlighted === 1) {
        return textOne;
      } else if (highlighted === 2) {
        return textTwo;
      } else if (highlighted === 3) {
        return textThree;
      } else if (highlighted === 4) {
        return textFour;
      }
    });
  }, [highlighted]);

  useEffect(() => {
    handleTitle();
    HandleExplainer();

    /* return cleanUp = () => {
      
    } */
  }, [handleTitle, HandleExplainer]);

  //Colors and text for Matrix
  const fill = 1;
  const strokeColor = "#f9f9f9";
  const strokeWidth = 8;
  const stringOne = "Lavthængende frugter";
  const stringTwo = "Hurtig gevinst";
  const stringThree = "Nødvendige forbedringer";
  const stringFour = "Langsigtede forbedringer";

  return (
    <div className="d-flex flex-col width-matrix height-auto just-cont-center align-items-center bg-color-light-gray padding-10-px box-shadow-card">
      <div className="d-flex flex-col align-items-center margin-1-0rem padding-0-1rem">
        <span className="d-flex just-cont-center font-fam-open-sans font-weight-sb margin-b-0-25rem text-align-center line-height-25px">
          Vi kan spare jer for:
        </span>
        <Output output={hoursSaved} label="Arbejdstimer årligt" />
        <Output output={moneySaved} label="Kroner årligt" />
      </div>
      <ResponsiveContainer width="100%" aspect={1 / 1}>
        <ScatterChart>
          <XAxis
            type="number"
            domain={[0, 5]}
            dataKey="x"
            name="Fordele"
            label={{
              value: "Fordele",
              fontFamily: "'Open Sans', sans-serif",
              fontSize: "14px",
            }}
            tick={false}
          />
          <YAxis
            type="number"
            domain={[0, 10]}
            dataKey="y"
            name="Kompleksitet"
            label={{
              value: "Kompleksitet",
              angle: -90,
              fontFamily: "'Open Sans', sans-serif",
              fontSize: "14px",
            }}
            width={30}
            tick={false}
          />
          <ReferenceArea
            x1={0}
            x2={2.5}
            y1={0}
            y2={5}
            stroke={strokeColor}
            strokeOpacity={1}
            strokeWidth={strokeWidth}
            fill={highlighted === 1 ? colorhighlight : colorDefault}
            fillOpacity={fill}
            label={{
              value: stringOne,
              fill: highlighted === 1 ? textColorHightlight : textColorDefault,
              fontFamily: "'Open Sans', sans-serif",
              fontWeight: "600",
              fontSize: "14px",
              width: 100,
            }}
          />
          <ReferenceArea
            x1={2.5}
            x2={5}
            y1={0}
            y2={5}
            stroke={strokeColor}
            strokeOpacity={1}
            strokeWidth={strokeWidth}
            fill={highlighted === 2 ? colorhighlight : colorDefault}
            fillOpacity={fill}
            label={{
              value: stringTwo,
              fill: highlighted === 2 ? textColorHightlight : textColorDefault,
              fontFamily: "'Open Sans', sans-serif",
              fontWeight: "600",
              fontSize: "14px",
              width: 100,
            }}
          />
          <ReferenceArea
            x1={2.5}
            x2={5}
            y1={5}
            y2={10}
            stroke={strokeColor}
            strokeOpacity={1}
            strokeWidth={strokeWidth}
            fill={highlighted === 3 ? colorhighlight : colorDefault}
            fillOpacity={fill}
            label={{
              value: stringThree,
              fill: highlighted === 3 ? textColorHightlight : textColorDefault,
              fontFamily: "'Open Sans', sans-serif",
              fontWeight: "600",
              fontSize: "14px",
              width: 100,
            }}
          />
          <ReferenceArea
            x1={0}
            x2={2.5}
            y1={5}
            y2={10}
            stroke={strokeColor}
            strokeOpacity={1}
            strokeWidth={strokeWidth}
            fill={highlighted === 4 ? colorhighlight : colorDefault}
            fillOpacity={fill}
            label={{
              value: stringFour,
              fill: highlighted === 4 ? textColorHightlight : textColorDefault,
              fontFamily: "'Open Sans', sans-serif",
              fontWeight: "600",
              fontSize: "14px",
              width: 100,
            }}
          />
          <Scatter data={data} fill="dark" />
        </ScatterChart>
      </ResponsiveContainer>
      <div className="d-flex flex-col padding-0-1rem margin-t-2rem margin-b-2rem">
        <span className="d-flex align-items-center font-fam-open-sans font-weight-b">
          {explainerTitle}
        </span>
        <p className="d-flex align-items-center just-cont-center font-fam-open-sans line-height-32px">
          {explainerText}
        </p>
      </div>
      <span className="d-flex just-cont-center font-fam-open-sans font-weight-sb margin-b-0-25rem text-align-center line-height-25px">
        Jeres investering vil være tjent hjem igen om:
      </span>
      <output className="d-flex just-cont-center font-fam-open-sans font-size-1-375rem font-weight-b text-align-center margin-b-1rem">
        {ROI} måneder*
      </output>
      <small className="d-flex font-fam-open-sans text-align-center margin-b-0-5rem">
        (* i forhold til vores gennemsnitlige projektpris)
      </small>
    </div>
  );
};

export default Matrix;
