const SubmitButton = ({ label }) => {
  return (
    <button
      type="submit"
      className="d-flex height-44px just-cont-center align-items-center font-fam-open-sans font-size-1-25rem bg-color-yellow color-dark border-style-none border-radius-submit-button padding-0-1rem cursor-pointer"
    >
      {label}
    </button>
  );
};

export default SubmitButton;
