const ContactEmail = ({ text, children }) => {
  return (
    <div className="d-flex flex-col">
      <span className="d-flex font-fam-open-sans font-weight-sb margin-t-0-5rem margin-b-1rem margin-left-0-25rem padding-0-1rem  line-height-25px">
        {text}
      </span>
      {children}
    </div>
  );
};

export default ContactEmail;
