const Results = ({ children }) => {
  return (
    <div className="d-flex flex-col width-results-lg align-items-center">
      {children}
    </div>
  );
};

export default Results;
