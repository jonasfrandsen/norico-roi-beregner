const ContactUs = ({ children }) => {
  return (
    <div className="d-flex flex-col width-contact-us bg-color-light-gray padding-10-px box-shadow-card">
      {children}
    </div>
  );
};

export default ContactUs;
