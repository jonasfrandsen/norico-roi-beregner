const EmailForm = ({ children }) => {
  return (
    <form
      //form is connected to Mailchimp account
      action="https://norico.us7.list-manage.com/subscribe/post"
      method="POST"
      className="d-flex width-100-percent margin-0-1rem"
    >
      {children}
    </form>
  );
};

export default EmailForm;
