const ButtonLink = ({ id, type, buttonLabel, labelColor, onClick }) => {
  return (
    <button
      id={id}
      type={type}
      onClick={onClick}
      className={`${labelColor} d-flex height-42px font-fam-open-sans font-size-1-125rem border-style-none bg-transparent text-decoration-underline margin-1-1rem cursor-pointer`}
    >
      {buttonLabel}
    </button>
  );
};

export default ButtonLink;
