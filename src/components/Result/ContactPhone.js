const ContactPhone = ({ text, phone }) => {
  return (
    <span className="d-flex font-fam-open-sans font-weight-sb margin-t-1rem margin-b-0-5rem margin-left-0-25rem padding-0-1rem line-height-25px">
      {text}
      <a
        href={`tel:${phone.replace(/\s/g, "")}`}
        className="d-flex font-fam-open-sans font-size-regular font-weight-b margin-l-1-ch"
      >
        {phone}
      </a>
    </span>
  );
};

export default ContactPhone;
