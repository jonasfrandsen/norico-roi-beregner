const EmailInput = ({
  id,
  name,
  type,
  value,
  setValue,
  hoursSaved,
  moneySaved,
  ROI,
  placeholder,
}) => {
  return (
    <div className="d-flex width-70-percent">
      {/* Hidden input fields are connected to mailchimp account */}
      <input type="hidden" name="u" value="9434de8d0c94d3d528aa8dca9" />
      <input type="hidden" name="id" value="4544c349f9"></input>
      <input
        id={id}
        name={name}
        type={type}
        value={value}
        placeholder={placeholder}
        onChange={(e) => setValue(e.target.value)}
        className="d-flex width-100-percent height-42px font-fam-open-sans font-size-1rem border-style-email-input border-radius-email-input 
      border-width-1px border-color-light-gray bg-color-light-gray padding-0-1-5rem margin-b-1rem"
      />
      <input id="MERGE1" name="MERGE1" type="hidden" value={hoursSaved} />
      <input id="MERGE2" name="MERGE2" type="hidden" value={moneySaved} />
      <input id="MERGE3" name="MERGE3" type="hidden" value={ROI} />
    </div>
  );
};

export default EmailInput;
